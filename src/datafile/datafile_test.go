package datafile

import (
	"fmt"
	"testing"
)

func Test(t *testing.T) {
	df, err := NewDataFile("test.txt", 10)
	if err != nil {
		t.Error(err.Error())
	}
	bytes := []byte{1, 2, 3, 4, 5, 6, 78, 8, 9}
	wsn, err := df.Write(bytes)
	if err != nil {
		t.Error(err.Error())
	}
	fmt.Println("The writing serial number is:", wsn)
}
