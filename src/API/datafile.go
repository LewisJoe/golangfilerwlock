package API

// 数据类型
type Data []byte

// 数据文件的接口类型
// wsn:Writing serial number,最后被写入的数据块的序列号
// rsn:reading serial number,最后被读取的数据块的序列号
type DataFile interface {

	// 读取一个数据块
	Read() (rsn int64, d Data, err error)

	// 写入一个数据块
	Write(d Data) (wsn int64, err error)

	// 获取最后读取的数据块的序列号
	Rsn() int64

	// 获取最后写入的数据块的序列号
	Wsn() int64

	// 获取数据块的长度
	DataLen() uint32
}
